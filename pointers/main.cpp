#include <iostream>
using namespace std;

struct Coordinate{
  int x;
  int y;

  Coordinate(int x, int y)
    : x(x), y(y)
  {}

  // Assignment operator
  Coordinate &operator = (const Coordinate *obj){
    x = obj->x;
    y = obj->y;
    return *this;
  }

  // Comparison operator
  // We don't need to chenge the object, so we
  // only need to pass value, not reference
  // We use const because we don't need to
  // modify our object
  bool operator == (Coordinate obj) const {
    return ((x == obj.x) && (y == obj.y));
  }

  // Addition operator 
  // We don't need to change the opbject, because
  // we create a new one.
  // Therefore -> const
  Coordinate operator + (Coordinate obj) const {
    return Coordinate(x + obj.x, y + obj.y);
  }
};


void addToVariableCCompliant(int *variable){
  *variable = *variable + 1;
}

void addToVariableCPPCompliant(int &variable){
  variable = variable + 1;
}


// Declaration, but function cannot be declared and initialized
// in the same place?!
void pointerFunction(int x){
  x = 4;
}

int main(){
  // int normalVariable = 4;
  // addToVariableCCompliant(&normalVariable);
  // cout << normalVariable << "\n";

  // int otherNormalVariable = 10;
  // addToVariableCPPCompliant(otherNormalVariable);
  // cout << otherNormalVariable << "\n";

  // Coordinate coordinate = Coordinate(10, 20);
  // Coordinate newCoordinate = Coordinate(20, 10);
  // coordinate = &newCoordinate;
  // cout << coordinate.x << ", " << coordinate.y << "\n";
  // if(coordinate == newCoordinate) cout << "Coordinates are equal!\n";
  // Coordinate additionResult = coordinate + newCoordinate;
  // cout << "Addition result:\n";
  // cout << additionResult.x << ", " << additionResult.y << "\n";
  
  void (*foo)(int);
  int bar = 2;

  // Declaration of foo as a pointer to the function
  foo = &pointerFunction;

  // We are calling the function "pointerFunction" from
  // foo, wich is a pointer to "pointerFunction".
  foo(2);

  // This and "foo(2)" are equivalent
  (*foo)(2);
}
