#pragma once
#include "model.h"
#include "view.h"

// Controller combines Model and View
class Controller {
  public:
    Controller(const Model &model, const View &view) {
      this->setModel(model);
      this->setView(view);
    }
    void setModel(const Model &model) { this->model = model; }
    void setView(const View &view) { this->view = view; }
    // When application starts
    void OnLoad() { this->view.Render(); }
  private:
    Model model;
    View view;
};
