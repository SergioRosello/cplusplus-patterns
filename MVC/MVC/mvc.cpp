#include <iostream>
#include "view.h"
#include "model.h"
#include "controller.h"
#include "common.h"

using namespace std;

void DataChange(string data) { cout << "Data Change: " << data << endl; }

int main() {
  Model model("Model");
  View view(model);
  // Register the data-change event
  model.RegisterDataChangeHandler(&DataChange);
  // Binds Model and View
  Controller controller(model, view);
  // When application starts or button is clicked, or form is shown...
  controller.OnLoad();
  // This should trigger view to render
  model.setData("Changes");
  return 0;
}
