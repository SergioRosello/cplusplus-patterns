#pragma once
#include <iostream>
#include "model.h"

class View {
  public:
    View(const Model &model) { this->model = model; }
    View() {}
    void setModel(const Model &model) {
      this->model = model;
    }
    void Render() { std::cout << "Model data = " << model.Data() << endl; }
  private:
    Model model;
};
