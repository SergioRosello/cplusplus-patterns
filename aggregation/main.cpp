#include <iostream>

#include "department.h"

int main(){
  Teacher *teacher = new Teacher("bob");
  {
    Department department(teacher);
  }

  std::cout << teacher->getName() << " Still exists!\n";

  delete teacher;

  return 0;
};
